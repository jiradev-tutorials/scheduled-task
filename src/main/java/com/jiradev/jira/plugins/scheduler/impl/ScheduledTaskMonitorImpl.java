package com.jiradev.jira.plugins.scheduler.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.jiradev.jira.plugins.scheduler.ScheduledTask;
import com.jiradev.jira.plugins.scheduler.ScheduledTaskMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;

@ExportAsService
@Component
public class ScheduledTaskMonitorImpl implements ScheduledTaskMonitor, LifecycleAware {

    /* package */ static final String KEY = ScheduledTaskMonitorImpl.class.getName() + ":instance";
    private static final String JOB_NAME = ScheduledTaskMonitorImpl.class.getName() + ":job";
    private long interval = 60000L;
    private Date lastRun = null;

    @JiraImport
    private final PluginScheduler pluginScheduler;

    @Autowired
    public ScheduledTaskMonitorImpl(PluginScheduler pluginScheduler) {
        this.pluginScheduler = pluginScheduler;
    }

    @Override
    public void onStart() {
        reschedule(interval);
    }

    public void reschedule(long interval) {
        this.interval = interval;

        pluginScheduler.scheduleJob(
                JOB_NAME,                   // unique name of the job
                ScheduledTask.class,     // class of the job
                new HashMap() {{
                    put(KEY, ScheduledTaskMonitorImpl.this);
                }},                         // data that needs to be passed to the job
                new Date(),                 // the time the job is to start
                interval);                  // interval between repeats, in milliseconds
    }

    /* package */ void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }

}