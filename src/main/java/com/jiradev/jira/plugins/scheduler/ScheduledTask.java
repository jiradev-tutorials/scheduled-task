package com.jiradev.jira.plugins.scheduler;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.sal.api.scheduling.PluginJob;
import java.util.Date;
import java.util.Map;

@Scanned
public class ScheduledTask implements PluginJob {

    public void execute(Map jobDataMap) {
        // Do what your scheduled task needs to do here
        // In this example, we simply print a line to the console
        System.out.println("================= This is a scheduled task (Run at " + new Date()+ " ) =====================");
    }
}