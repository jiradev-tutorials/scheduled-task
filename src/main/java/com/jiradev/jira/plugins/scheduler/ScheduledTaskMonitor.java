package com.jiradev.jira.plugins.scheduler;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

@Scanned
public interface ScheduledTaskMonitor {
    public void reschedule(long interval);
}